/**
 * Created by Tianshaokai on 2014/12/19.
 */
var models = require('../models');
var AdminUser = models.AdminUser;
var Utils = require('../utils/utils');
var log4js = require('log4js');
var logger = log4js.getLogger('normal');


exports.getUserByName = function(userName, filed, callback) {
    AdminUser.findOne({'userName': userName}, filed, callback);
};

/**
 *   获取用户名和密码
 * @param userName  用户名
 * @param pwd  密码
 * @param callback  回调函数
 */
exports.getUserByNameAndPasswd = function(userName, password, fileds, callback) {
    logger.debug('begin to getUserByNameAndPasswd. username ' + userName + ' password ' + password);
    AdminUser.findOne({'userName': userName, 'password': Utils.md5(password)}, fileds, callback);
};

/**
 *   注册用户
 * @param userName  用户名
 * @param pwd  密码
 * @param callback  回调函数
 */
exports.register = function(userName, password, callback) {

    logger.debug('begin to register.');
    var adminuser = new AdminUser();
    adminuser.userName = userName;
    adminuser.password = Utils.md5(password);
    adminuser.save(callback);
};