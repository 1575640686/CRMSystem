/**
 * Created by Tianshaokai on 2014/12/19.
 */
var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var config = require('app-config').config;
var utils = require('../utils/utils');

/**
 *  管理员Model
 * @type {Schema}
 */
var AdminUserSchema = new Schema ({

    userName: { type: String, unique: true },           // 用户名
    password: { type: String },                         // 密码
    create_at: { type: Date, default: Date.now },       // 创建日期
    update_at: { type: Date, default: Date.now },       // 更新日期
    last_login_time:{type: Date, default: Date.now},    // 最后登录时间
    last_ip : {type: String}                            // 用户最后登录的ip地址
},{ strict: false });

mongoose.model('AdminUser', AdminUserSchema);