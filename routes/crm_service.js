/**
 * Created by tianshaokai on 2014/12/11.
 */
var config = require('app-config').config;
var log4js = require('log4js');
var logger = log4js.getLogger('normal');
var utils = require('../utils/utils');
var adminUser = require('../controller/admin_user');
var url = require('url');

var crm = {};

exports.action = function(req, res, next){

    var command = req.param('command');

    var device = req.param('device') || 1;

    if(command == null || (device != 1 && device != 2)) {
        sendRes(config.config.returncode.paramswrong, res);
    } else {
        try {
            if(crm[command] == undefined) {
                sendRes(config.config.returncode.commandnotfound, res);
            } else {
                crm[command](req, res);
            }
        } catch (error) {
            console.log('请求异常: ' + error);
            sendRes(error, res);
        }
    }
};

crm.index = function(req, res) {
    res.render('user/login', { title: 'Express' });
}

crm.getToken = function(req, res) {
    sendRes({code: 0, token: '123'}, res);
}

/**
 *  注册 用户
 * @param req
 * @param res
 */
crm.register = function(req, res) {
    adminUser.register(req, function(sendResponse) {
        sendRes(sendResponse, res);
    });
}

/**
 *  用户 登录
 * @param req
 * @param res
 */
crm.login = function(req, res) {
    adminUser.login(req, function(sendResponse) {
        sendRes(sendResponse, res);
    });
}


//发送数据
var sendRes = function(sendResponse, res) {
    res.send(sendResponse);
};