/**
 * Created by Administrator on 2014/12/11.
 */
var config = {
    returncode: {
        right: {code: 0, msg: ""},
        paramswrong: {code: '10000', message: "参数错误"},
        commandnotfound: {code: 2000, message: '命令不存在'},
        expiresout: {code: 2001, message: '用户失效,请重新登录'},
        notLogin: {code: 2002, message: '用户未登录'},
        userRegistered: {code: 2003, message: '用户已注册'},
        nameOrPassWrong:{code:2010, message:'用户名或者密码不正确'},
        databaseerr: {code: 3000, message: '数据库错误'},

        findWorksFail: {code: 10007, message: '请求作品列表失败'},

        topicnotfound: {code: 4000, message: '文章不存在'},
        topicIdBlank: {code: 4001, message: '文章id不能为空'},
        queryTopicFail: {code: 4002, message: '请求文章列表失败'},
        queryOneTopicFail: {code: 4003, message: '请求文章失败'},
        commentblank: {code: 5000, message: '评论内容不能为空'},
        commentFail: {code: 5001, message: '发表评论失败'},
        delCommentFail: {code: 5002, message: '删除评论失败'},
        getTopicCommentFail: {code: 5003, message: '获取文章评论失败'},
        getuserinfofail: {code: 6000, message: '获取用户信息失败'},
        userNotExit: {code: 6001, message: '用户不存在'},
        userIdBlank: {code: 6002, message: '用户id不能为空'},
        userBgImgBlank: {code: 6003, message: '背景图片不能为空'},
        getStarUserFail: {code: 6004, message: '获取达人失败'},
        getPraiseUserFail: {code: 6005, message: '获取文章赞用户列表失败'},
        followIdBlank: {code: 6100, message: '关注者id不能为空'},
        followFail: {code: 6200, message: '关注失败'},
        unFollowFail: {code: 6201, message: '取消关注失败'},
        praiseTopicErr: {code: 6300, message: '赞文章失败'},
        unPraiseTopicErr: {code: 6301, message: '取消赞文章失败'},
        praiseGameErr: {code: 6400, message: '赞游戏失败'},
        unPraiseGameErr: {code: 6401, message: '取消赞游戏失败'},
        praiseFMErr: {code: 6500, message: '赞朋友文章失败'},
        unPraiseFMErr: {code: 6501, message: '取消赞朋友文章失败'},
        gameNotFound: {code: 7000, message: '游戏不存在'},
        gameIdBlank: {code: 7001, message: '游戏id不能为空'},
        gameCollectFail: {code: 7002, message: '收藏游戏失败'},
        gameCollectSuccess: {code: 7003, message: '收藏游戏成功'},
        delGameCollectFail: {code: 7004, message: '删除收藏游戏失败'},
        getCollectGameFail: {code: 7005, message: '获取收藏游戏失败'},
        delGameCollectSuccess: {code: 7006, message: '删除收藏游戏成功'},
        getGameInfoFail: {code: 7007, message: '获取游戏失败'},
        getHotGameInfoFail: {code: 7008, message: '获取最热游戏失败'},
        countFail: {code: 7100, message: '统计失败'},
        publicMessageErr: {code: 8000, message: '文章发布失败'},
        imgUploadErr: {code: 8001, message: '图片上传失败'},
        contentBlank: {code: 8002, message: '发表内容不能为空'},
        transferTopicTypeWrong: {code: 8003, message: '转发类型不正确'},
        transferTopicFail: {code: 8003, message: '转发文章失败'},
        findFriendsFail: {code: 9000, message: '获取好友失败'},
        findFriendMessageFail: {code: 9001, message: '获取好友信息失败'},
        getFollowerFail: {code: 9002, message: '获取我的关注失败'},
        getFansFail: {code: 9003, message: '获取我的粉丝失败'},
        getGiftFail: {code: 9004, message: '获取我的礼包失败'},
        getGiftCodeFail: {code: 9005, message: '获取礼包激活码失败'},
        getAllGiftFail: {code: 9100, message: '获取礼包失败'},
        getAllGiftExists: {code: 9101, message: '已经获取过礼包'},
        GiftIsOut: {code: 9102, message: '礼包已经发放完毕'},
        giftNotExist: {code: 9103, message: '礼包不存在'}

    },

    db: "mongodb://@127.0.0.1:27017/CRMSystem",
    port: 31000,
    db_name: 'CRMSystemSession',
    db_seesion_user:'crm',
    db_seesion_pass:'crm',

    session_secret: 'CRMSystem',

    debug: true




};

module.exports = config;
module.exports.config = config;