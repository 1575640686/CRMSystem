/**
 * Created by Administrator on 2015/5/9.
 */
var crmService = angular.module("CrmService", []);

crmService.factory(function($http,$q){
    return {
        login: function(httpParams){
            var deferred = $q.defer();
            $http({
                method:"post",
                url: "/angular-war/loginServlet",
                params:httpParams
            }).success(function(data, status) {
                deferred.resolve(data);
            }).error(function(data, status) {
                deferred.reject(null);
            });
            return deferred.promise;
        }
    };
});