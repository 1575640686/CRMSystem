/**
 * Created by tianshaokai on 2014/12/14.
 */
var mongoose = require('mongoose');
var config = require('app-config').config;

mongoose.connect(config.db, function(err) {
    if(err) {
        console.log('connect to %s error: ', config.db, err.message);
        process.exit(1);
    }
});

// 引入model
require('./admin_user');

exports.AdminUser = mongoose.model('AdminUser');