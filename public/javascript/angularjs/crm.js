/**
 * Created by TianShaoKai on 2014/12/12.
 */
var crmSystem = angular.module("CRMSystem", ["ui.router", "CrmController"]);

crmSystem.config(function($stateProvider, $urlRouterProvider) {
    $urlRouterProvider.otherwise("/login");

    $stateProvider
        .state('login',{
            url: "/login",
            views: {
                "": {
                    templateUrl: "tpls/login.html"
                }

            }
        })
        .state('index',{
            url: "/index",
            views: {
                "": {
                    templateUrl: "tpls/index.html"
                }

            }
        })
});
