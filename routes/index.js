/**
 * Created by Administrator on 2014/12/11.
 */
var express = require('express');
var crm = require('./crm_service');

var router = express.Router();

router.all('/crm', crm.action);

module.exports = router;