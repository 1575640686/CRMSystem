/**
 * Created by tianshaokai on 2014/12/12.
 */
var crmController = angular.module("CrmController", []);

crmController.controller("loginCtoller", ["$scope", "$http", "$state", function($scope, $http, $state) {
    $scope.login = function() {
        $http({method: 'POST',
                data: {"userName": $scope.userName, "password": $scope.password},
                url: 'http://localhost:31000/crm?command=login&device=2'})
            .success(function(data, status, headers, config){
                if(status == 200) {
                    console.log(data);
                    if(data.code == 0) {
                        $state.go('index');
                    } else {
                        $state.go('login');
                    }

                }
            })
            .error(function(data, status, headers, config){
                console.log("failure");
                console.log(data);
            });
    }
}]);

crmController.controller("registerCtrl", ["$scope", "$http", function($scope, $http) {

    $http.register = function() {
        $http({method: 'POST',
                data: {"userName": $scope.userName, "password": $scope.password},
                url: 'http://localhost:31000/crm?command=register'})
            .success(function(data, status, headers, config) {
                console.log("success");
            })
            .error(function(data, status, headers, config) {
                console.log("failure");
            });
    };
}]);